import Router from 'koa-router'
const books = new Router()

import * as boosController from './books.controller.js'

// 핸들러는 분리하여 사용 controller 생성
// const handler = (ctx) => {
// 	ctx.body = `${ctx.request.method} ${ctx.request.path}`
// }

books.get('/:id', boosController.get)
books.get('/', boosController.list)
books.post('/', boosController.store)
books.delete('/', boosController.destroy)
books.put('/', boosController.replace)
books.patch('/', boosController.update)

export default books
