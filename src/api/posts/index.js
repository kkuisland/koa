import Router from 'koa-router'
const posts = new Router()

import * as postsController from './posts.controller.js'

posts.get('/', postsController.list)
posts.post('/', postsController.create)
posts.delete('/', postsController.destroy)
posts.put('/', postsController.replace)
posts.patch('/', postsController.update)

export default posts
