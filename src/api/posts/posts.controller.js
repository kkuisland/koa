/*
	Firebase
	*/
import {v4 as uuidv4} from 'uuid'
import admin from 'firebase-admin'
import serviceAccount from '../../../firebase.json'
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	storageBucket: 'kkustagram.appspot.com',
})
const db = admin.firestore()
let bucket = admin.storage().bucket()

/*
	Routes
	*/
export const list = async (ctx) => {
	// ctx.body = 'listed'
	ctx.response.set('Access-Control-Allow-Origin', '*')

	let posts = []
	await db
		.collection('posts')
		.orderBy('date', 'desc')
		.get()
		.then((snapshot) => {
			snapshot.forEach((doc) => {
				console.log(doc.id, '=>', doc.data())
				posts.push(doc.data())
			})
		})

	ctx.body = await posts
}

export const create = (ctx) => {
	ctx.response.set('Access-Control-Allow-Origin', '*')
	const fileData = ctx.request.files.file
	const uuid = uuidv4()
	// console.log(fileData)
	// const name = fileData.path + '/' + fileData.name
	// console.log(name)
	// Firebase storage
	bucket.upload(
		fileData.path,
		{
			uploadType: 'media',
			metadata: {
				contentType: fileData.type,
				metadata: {
					firebaseStorageDownloadTokens: uuid,
				},
			},
		},
		(error, uploadFile) => {
			console.log(uploadFile)
			if (!error) {
				const newData = {
					...ctx.request.body,
					date: parseInt(ctx.request.body.date),
					imageUrl: `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${uploadFile.name}?alt=media&token=${uuid}`,
				}
				// Firestore Database
				db.collection('posts').doc(ctx.request.body.id).set(newData)
			}
		}
	)
}

export const destroy = (ctx) => {
	ctx.body = 'destroyed'
}

export const replace = (ctx) => {
	ctx.body = 'replaced'
}

export const update = (ctx) => {
	ctx.body = 'updated'
}
