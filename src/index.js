/* 
	선언
*/
// Config
// require('dotenv').config() // .env 파일에서 환경변수 불러오기
import Dotenv from 'dotenv'
Dotenv.config()
// Koa
import Koa from 'koa'
import Router from 'koa-router'
// 변수
const app = new Koa()
const router = new Router()
const port = process.env.PORT || 3000 // PORT 값이 없으면 3001 포트 사용
// 모듈
import api from './api/index.js'
// MongoDB
import mongoose from 'mongoose'
import bodyParser from 'koa-bodyparser'
import formidable from 'koa2-formidable'

/* 
	이제, bodyParser 를 적용하였으니, 다음과 같은 형식으로 body 에 있는 JSON 객체를 사용 할 수 있게 됩니다.

	app.use(async ctx => {
		// 아무것도 없으면 {} 가 반환됩니다.
		ctx.body = ctx.request.body;
	});
*/

mongoose.Promise = global.Promise // Node 의 네이티브 Promise 사용
// MongoDB 연결
mongoose
	.connect(process.env.MONGO_URI, {
		// auth: {
		// 	authdb: 'admin',
		// },
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then((response) => {
		// 에러 방지
		if (response) console.log('Successfully connected to mongodb =>')
	})
	.catch((error) => {
		console.log(error)
	})
// const Schema = mongoose.Schema

// const blogSchema = new Schema({
// 	title: String,
// 	author: String,
// 	body: String,
// 	comments: [{body: String, date: Date}],
// 	date: {type: Date, default: Date.new},
// 	hidden: Boolean,
// 	meta: {
// 		votes: Number,
// 		favs: Number,
// 	},
// })
// const Blog = mongoose.model('Blog', blogSchema)

/* 
	Route 연습
*/
router.get('/', (ctx) => {
	ctx.body = '홈'
})
router.get('/about', (ctx) => {
	ctx.body = '소개'
})
router.get('/about/:name/:phone', (ctx) => {
	// 라우트 경로에서 :파라미터명 으로 정의된 값이 ctx.params 안에 설정됩니다.
	const {name, phone} = ctx.params
	ctx.body = `${name}의 소개 ${phone}`
})
router.get('/post', (ctx) => {
	// 주소 뒤에 ? id=10 이런식으로 작성된 쿼리는 ctx.request.query 에 파싱 됩니다.
	const {id} = ctx.request.query
	if (id) {
		ctx.body = `포스트 #${id}`
	} else {
		ctx.body = `포스트 아이디가 없습니다.`
	}
})
/* 
	Router Api 연결 
*/
app.use(formidable())
app.use(bodyParser()) // 바디파서 적용, 라우터 적용코드보다 상단에 있어야 합니다.
router.use('/api', api.routes()) // api 라우트를 /api 경로 하위 라우트로 설정
/*
Router Option
*/
// 모듈 api
app.use(router.routes()).use(router.allowedMethods())
/* 
	Listen 
*/
app.listen(port, () => {
	console.log(`KKuIsland KoaServer: ${port} Open!`)
})
